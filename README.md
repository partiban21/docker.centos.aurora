# docker.centos.aurora

Docker image to test server-side scripting

Includes languages:

- Python (*.py) (examples: Pyramid, Flask, Django)
- Ruby (*.rb, *.rbw) (example: Ruby on Rails)   
