FROM centos:7

ARG PYENV_ROOT="/root/.pyenv"
ARG PYENV_VIRTUAL_ENV_PLUGIN=$PYENV_ROOT/plugins/pyenv-virtualenv
ARG RBENV_ROOT="/root/.rbenv"
ARG RBENV_RUBY_BUILD_PLUGIN=$RBENV_ROOT/plugins/ruby-build

RUN yum clean all \
    && yum install --setopt=skip_missing_names_on_install=False -y \
        bzip2 \
        bzip2-devel \
        epel-release \
        gcc \
        gcc-c++ \
        gettext \
        git \
        make \
        openssl \
        openssl-devel \
        python36 \
        python36-pip \
        readline-devel \
        ruby \
        sqlite \
        sqlite-devel \
        tree \
        vim \
        which \
        zlib-devel

# Install Pyenv
RUN git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT \
    && git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_VIRTUAL_ENV_PLUGIN \
    && echo "export PYENV_ROOT=\"$PYENV_ROOT\"" >> /root/.bashrc \
    && echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> /root/.bashrc \
    && echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> /root/.bashrc \
    && source /root/.bashrc \
    && echo 'eval "$(pyenv virtualenv-init -)"' >> /root/.bashrc \
    && source /root/.bashrc \
    && pyenv install 3.6.7 \
    && pyenv install 2.7.15 \
    && pyenv global 3.6.7

# Install rbenv
RUN git clone https://github.com/rbenv/rbenv.git $RBENV_ROOT \
    && echo "export RBENV_ROOT=\"$RBENV_ROOT\"" >> /root/.bashrc \
    && echo 'export PATH="$RBENV_ROOT/bin:$PATH"' >> /root/.bashrc \
    && echo -e 'if command -v rbenv 1>/dev/null 2>&1; then\n  eval "$(rbenv init -)"\nfi' >> /root/.bashrc \
    && echo 'eval "$(rbenv init -)"' >> /root/.bashrc \
    && git clone https://github.com/rbenv/ruby-build.git $RBENV_RUBY_BUILD_PLUGIN \
    && source /root/.bashrc \
    && rbenv install 2.3.8 \
    && rbenv install 2.6.4 \
    && rbenv global 2.6.4

ENV PATH="$RBENV_ROOT/shims:$RBENV_ROOT/bin:$PYENV_VIRTUAL_ENV_PLUGIN/shims:$PYENV_ROOT/shims:$PYENV_ROOT/bin:${PATH}"
